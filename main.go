package main

import (
	"context"
	"errors"
	"fmt"
	"os"
	"time"

	"github.com/alecthomas/kingpin"
	"github.com/sethvargo/go-retry"
	"github.com/xanzy/go-gitlab"
)

var (
	gitlabAPIBaseURL = kingpin.Flag("gitlab-api-base-url", "API base URL for the GitLab instance on the mirror pipeline is running.").
				Envar("GITLAB_API_BASE_URL").Default("https://gitlab.com/api/v4").String()
	gitlabAPIToken = kingpin.Flag("gitlab-api-token", "API token for the GitLab instance on which the mirror pipeline is running.").
			Envar("GITLAB_API_TOKEN").Required().String()
	mirrorProjectPath = kingpin.Flag("mirror-project-path", "Project path of mirror. Defaults to the same project path as the CI job, useful for cross-instance linking with identical paths.").
				Envar("MIRROR_PROJECT_PATH").Default("").String()
	remotePipelineTimeoutSeconds = kingpin.Flag("remote-pipeline-timeout-seconds", "Remote pipeline timeout seconds.").
					Envar("REMOTE_PIPELINE_TIMEOUT_SECONDS").Default("3600").Int()
)

func main() {
	kingpin.Parse()

	projPath := *mirrorProjectPath
	if projPath == "" {
		projPath = os.Getenv("CI_PROJECT_PATH")
	}
	fmt.Println("Using project path " + projPath)

	commitSHA := os.Getenv("CI_COMMIT_SHA")
	if commitSHA == "" {
		fmt.Println("Must set CI_COMMIT_SHA: is this not running in GitLab CI?")
		os.Exit(1)
	}
	fmt.Println("Waiting for a pipeline to be created for this commit")

	client, err := gitlab.NewClient(*gitlabAPIToken, gitlab.WithBaseURL(*gitlabAPIBaseURL))
	fatal(err)

	pipelines, err := awaitNonzeroPipelineCount(client, projPath, commitSHA)
	fatal(err)

	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(*remotePipelineTimeoutSeconds)*time.Second)
	defer cancel()

	pipelineStatuses := make(chan *gitlab.Pipeline)
	for _, pipeline := range pipelines {
		fmt.Println("monitoring pipeline: " + pipeline.WebURL)
		go monitorPipeline(ctx, client, projPath, pipeline, pipelineStatuses)
	}

	pipelinesRemaining := len(pipelines)
	exitCode := 0
	for {
		select {
		case pipelineComplete := <-pipelineStatuses:
			fmt.Printf("%s completed with status: %s\n", pipelineComplete.WebURL, pipelineComplete.Status)
			if pipelineComplete.Status != "success" {
				exitCode = 1
			}
			pipelinesRemaining--
			if pipelinesRemaining == 0 {
				os.Exit(exitCode)
			}
		case <-ctx.Done():
			fmt.Println(ctx.Err())
			os.Exit(1)
		}
	}
}

func monitorPipeline(ctx context.Context, client *gitlab.Client, projPath string, pipeline *gitlab.PipelineInfo, pipelineStatuses chan<- *gitlab.Pipeline) {
	var latestPipeline *gitlab.Pipeline
	_ = retry.Constant(ctx, time.Second*10, func(ctx context.Context) error {
		var err error
		latestPipeline, _, err = client.Pipelines.GetPipeline(projPath, pipeline.ID)
		if err != nil {
			return retry.RetryableError(err)
		}
		if !isTerminalStatus(latestPipeline.Status) {
			return retry.RetryableError(fmt.Errorf("pipeline %s status: %s", latestPipeline.WebURL, latestPipeline.Status))
		}
		return nil
	})
	pipelineStatuses <- latestPipeline
}

func awaitNonzeroPipelineCount(client *gitlab.Client, projPath, commitSHA string) ([]*gitlab.PipelineInfo, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*5)
	defer cancel()
	var pipelines []*gitlab.PipelineInfo
	err := retry.Constant(ctx, time.Second*10, func(ctx context.Context) error {
		var err error
		pipelines, _, err = client.Pipelines.ListProjectPipelines(projPath, &gitlab.ListProjectPipelinesOptions{SHA: &commitSHA})
		if err != nil {
			return retry.RetryableError(err)
		}
		if len(pipelines) == 0 {
			return retry.RetryableError(errors.New("zero pipelines found"))
		}
		return nil
	})
	return pipelines, err
}

// https://docs.gitlab.com/ee/api/pipelines.html#list-project-pipelines
func isTerminalStatus(pipelineStatus string) bool {
	for _, terminalStatus := range []string{"success", "failed", "canceled", "skipped"} {
		if pipelineStatus == terminalStatus {
			return true
		}
	}
	return false
}

func fatal(err error) {
	if err != nil {
		panic(err)
	}
}
