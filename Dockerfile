FROM golang:1.15
WORKDIR /app
COPY . /app
RUN CGO_ENABLED=0 go build

FROM alpine:latest
COPY --from=0 /app/cross-project-pipelines /bin/cross-project-pipelines
CMD ["/bin/cross-project-pipelines"]
