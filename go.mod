module gitlab.com/cross-project-pipelines/cross-project-pipelines

go 1.15

require (
	github.com/alecthomas/kingpin v2.2.6+incompatible
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751 // indirect
	github.com/alecthomas/units v0.0.0-20190924025748-f65c72e2690d // indirect
	github.com/hashicorp/go-retryablehttp v0.6.7 // indirect
	github.com/sethvargo/go-retry v0.1.0
	github.com/xanzy/go-gitlab v0.37.0
	golang.org/x/net v0.0.0-20200904194848-62affa334b73 // indirect
	golang.org/x/oauth2 v0.0.0-20200902213428-5d25da1a8d43 // indirect
	golang.org/x/time v0.0.0-20200630173020-3af7569d3a1e // indirect
)
