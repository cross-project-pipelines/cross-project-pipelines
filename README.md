# cross-project-pipelines

When run inside a GitLab CI job, this will block until another GitLab CI
pipeline finishes, possibly on another GitLab instance. The job will succeed or
fail based on whether or not that remote pipeline succeeds. This is useful for
ensuring that MRs are associated with remote (mirrored) pipeline runs and that
broken code isn't accidentally merged.

## Usage

See [an example CI configuration](https://gitlab.com/cross-project-pipelines/test-project/-/blob/master/.gitlab-ci.yml):

```yaml
---
stages:
  - remote
  - test
  - deploy

await_remote:
  stage: remote
  image: registry.gitlab.com/cross-project-pipelines/cross-project-pipelines
  script: cross-project-pipelines --mirror-project-path cross-project-pipelines/test-project-mirror
  rules:
    - if: '$RUN_CI != "1"'

test:
  stage: test
  image: alpine
  script: echo "testing commit ${CI_COMMIT_SHORT_SHA}"
  rules:
    - if: '$RUN_CI == "1"'

deploy:
  stage: deploy
  image: alpine
  script: echo "deploying commit ${CI_COMMIT_SHORT_SHA}"
  rules:
    - if: '$RUN_CI == "1" && $CI_COMMIT_BRANCH == "master"'
```

In this case, the `await_remote job` is running `cross-project-pipelines`,
following a mirror on the same GitLab instance (gitlab.com). The test and deploy
jobs only run on the remote mirror.

`--mirror-project-path` defaults to the same project path as the CI job is
running for, which is useful for following remote pipelines on a separate
instance, but with identical project paths. You can override the instance from
its default of gitlab.com with `--gitlab-api-base-url`.

You can also see this in action on this MR:
https://gitlab.com/cross-project-pipelines/test-project/-/merge_requests/1.

Run `cross-project-pipelines --help` (or `docker run --rm
registry.gitlab.com/cross-project-pipelines/cross-project-pipelines
cross-project-pipelines --help`) to see complete configuration options including
defaults, or view the options in [`main.go`](main.go).

`GITLAB_API_TOKEN` is a secret and should be configured as a masked CI variable,
but not a protected one if you want to follow branch pipelines. The token must
have [`read_api`
scope](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#limiting-scopes-of-a-personal-access-token).
